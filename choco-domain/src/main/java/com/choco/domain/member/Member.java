package com.choco.domain.member;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@ToString
@TableGenerator(
        name = "chocoSequenceGenerator",
        table = "choco.choco_index",
        pkColumnName = "sequenceName",
        pkColumnValue = "seq_member",
        valueColumnName = "nextValue",
        allocationSize = 1
)
@EqualsAndHashCode(of = "memberSrl")
@Table(name = "member")
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "chocoSequenceGenerator")
    @Column(name = "memberSrl")
    private Long memberSrl;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Lob
    @Column(name = "description")
    private String description;

    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @Column(name = "modifiedAt")
    private LocalDateTime modifiedAt;

    public Member() {
    }

    @Builder
    public Member(String name, Integer age, Gender gender, String description, LocalDateTime createdAt, LocalDateTime modifiedAt) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.description = description;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public enum Gender {
        FMALE,
        MALE
    }
}

