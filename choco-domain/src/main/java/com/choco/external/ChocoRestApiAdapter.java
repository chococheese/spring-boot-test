package com.choco.external;

import com.google.common.base.Throwables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class ChocoRestApiAdapter {
    @Autowired
    private RestTemplate chocoRestTemplate;

    public String getHtmlPage(String url) {
        try {
            ResponseEntity<String> response = chocoRestTemplate.getForEntity(url, String.class);
            if (response == null) {
                throw new IllegalStateException("REST API response is null. url: " + url);
            }
            HttpStatus code = response.getStatusCode();
            if (code.is2xxSuccessful()) {
                return response.getBody();
            }
            throw new IllegalStateException("REST API response code is not success. url: " + url + ", code: " + code.value());
        } catch (RestClientException rce) {
            throw Throwables.propagate(rce);
        }
    }
}
