package com.choco.utils;

import org.slf4j.helpers.MessageFormatter;

public class ChocoStringUtils {
    public static String csf(String messagePattern, Object... args) {
        return MessageFormatter.arrayFormat(messagePattern, args).getMessage();
    }
}
