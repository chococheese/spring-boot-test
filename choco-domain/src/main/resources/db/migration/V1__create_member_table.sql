USE choco;

CREATE TABLE choco.choco_index (
	sequenceName VARCHAR(255) NOT NULL PRIMARY KEY,
	nextValue BIGINT
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='primary key sequence 증가를 위한 테이블';

CREATE TABLE choco.member (
	`memberSrl` BIGINT NOT NULL COMMENT 'member 시리얼 넘버',
	`name` VARCHAR(50) NOT NULL COMMENT '이름',
	`age` INT COMMENT '나이',
	`gender` VARCHAR(15) COMMENT '성별. (FMALE, MALE)',
	`description` TEXT COMMENT '특이사항',
	`createdAt` DATETIME(6) NOT NULL COMMENT '가입일자',
	`modifiedAt` DATETIME(6) NOT NULL COMMENT 'member 정보 변경 일자',
	PRIMARY KEY (`memberSrl`),
	KEY `idx_name` (`name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='member 테이블';
