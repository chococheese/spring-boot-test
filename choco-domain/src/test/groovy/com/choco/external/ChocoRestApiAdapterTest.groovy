package com.choco.external

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class ChocoRestApiAdapterTest extends Specification {
    @Subject
    ChocoRestApiAdapter dut
    RestTemplate chocoRestTemplate = Mock()

    void setup() {
        dut = new ChocoRestApiAdapter(
                chocoRestTemplate: chocoRestTemplate
        )
    }

    def "trivial test"() {
        given:
        1 == 1

        when:
        1 == 1

        then:
        1 == 1
    }

    def "getHtmlPage"() {
        given:
        String anyUrl = "https://www.naver.com"

        when:
        String expect = dut.getHtmlPage(anyUrl)

        then:
        1 * chocoRestTemplate.getForEntity(anyUrl, String) >> new ResponseEntity<>("<html><body>hello</body></html>", HttpStatus.OK)
        expect == "<html><body>hello</body></html>"
    }

    @Unroll
    def "getHtmlPage - #description"() {
        given:
        String anyUrl = "https://www.naver.com"

        and:
        1 * chocoRestTemplate.getForEntity(anyUrl, String) >> response

        when:
        dut.getHtmlPage(anyUrl)

        then:
        IllegalStateException exception = thrown(IllegalStateException)
        exception.message == expect

        where:
        response                                                     || expect                                                                         | description
        null                                                         || "REST API response is null. url: https://www.naver.com"                        | "response 가 null 이면 예외를 던진다"
        new ResponseEntity<>("404 not found.", HttpStatus.NOT_FOUND) || "REST API response code is not success. url: https://www.naver.com, code: 404" | "Http 상태 코드값이 200번대가 아니면 예외를 던진다"
    }

    def "getHtmlPage - REST 통신중 예외 발생시 상위로 전파한다"() {
        given:
        String anyUrl = "https://www.naver.com"

        and:
        1 * chocoRestTemplate.getForEntity(anyUrl, String) >> {
            throw new RestClientException("API 통신 오류 발생")
        }

        when:
        dut.getHtmlPage(anyUrl)

        then:
        RestClientException exception = thrown(RestClientException)
        exception.message == "API 통신 오류 발생"
    }
}
