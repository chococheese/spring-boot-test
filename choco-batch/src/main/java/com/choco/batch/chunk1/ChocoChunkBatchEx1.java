package com.choco.batch.chunk1;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.choco.utils.ChocoStringUtils.csf;

@Slf4j
@Component
public class ChocoChunkBatchEx1 {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Autowired
    public ChocoChunkBatchEx1(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Job chocoChunkBatchEx1Job() {
        return this.jobBuilderFactory.get("#ChocoChunkBatchEx1_Job")
                .start(chocoChunkBatchEx1Step())
                .build();
    }

    @Bean
    protected Step chocoChunkBatchEx1Step() {
        ChocoChunkBatchEx1CustomItemReader reader = new ChocoChunkBatchEx1CustomItemReader();
        ChocoChunkBatchEx1CustomItemProcessor processor = new ChocoChunkBatchEx1CustomItemProcessor();
        ChocoChunkBatchEx1CustomItemWriter writer = new ChocoChunkBatchEx1CustomItemWriter();

        TaskletStep step1 = this.stepBuilderFactory.get("#ChocoChunkBatchEx1_Step").<String, String>chunk(3)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();

        step1.setAllowStartIfComplete(true);

        return step1;
    }

    public static class ChocoChunkBatchEx1CustomItemReader implements ItemReader<String> {
        private final List<String> ALPHABETS = Lists.newArrayList("a", "b", "c", "d", "e", "f", "g", "h");
        private int fetchCount = 0;

        @Override
        public String read() throws ParseException, NonTransientResourceException {
            if (fetchCount < this.ALPHABETS.size()) {
                return this.ALPHABETS.get(fetchCount++);
            } else {
                fetchCount = 0;
                return null;
            }
        }
    }

    public static class ChocoChunkBatchEx1CustomItemWriter implements ItemWriter<String> {
        @Override
        public void write(List<? extends String> items) {
            for (String item : items) {
                log.info(csf("[ChocoChunkBatchEx1CustomItemWriter] item: {}", item));
            }
        }
    }

    public static class ChocoChunkBatchEx1CustomItemProcessor implements ItemProcessor<String, String> {
        @Override
        public String process(String item) {
            return item.toUpperCase();
        }
    }
}
