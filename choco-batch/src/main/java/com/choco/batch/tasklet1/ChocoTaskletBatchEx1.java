package com.choco.batch.tasklet1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ChocoTaskletBatchEx1 {
//    private final JobBuilderFactory jobBuilderFactory;
//    private final StepBuilderFactory stepBuilderFactory;
//
//    @Autowired
//    public ChocoTaskletBatchEx1(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory) {
//        this.jobBuilderFactory = jobBuilderFactory;
//        this.stepBuilderFactory = stepBuilderFactory;
//    }
//
//    @Bean
//    public Job chocoTaskletBatchEx1Job() {
//        return this.jobBuilderFactory.get("#ChocoTaskletBatchEx1_Job")
//                .start(chocoTaskletBatchEx1Step1())
//                .next(chocoTaskletBatchEx1Step2())
//                .build();
//    }
//
//    @Bean
//    public Step chocoTaskletBatchEx1Step1() {
//        TaskletStep step1 = this.stepBuilderFactory.get("#ChocoTaskletBatchEx1_Step1")
//                .tasklet(chocoTaskletBatchEx1Tasklet1())
//                .build();
//
//        step1.setAllowStartIfComplete(true);
//
//        return step1;
//    }
//
//    @Bean
//    public Step chocoTaskletBatchEx1Step2() {
//        TaskletStep step2 = this.stepBuilderFactory.get("#ChocoTaskletBatchEx1_Step2")
//                .tasklet(chocoTaskletBatchEx1Tasklet2())
//                .build();
//
//        step2.setAllowStartIfComplete(true);
//
//        return step2;
//    }
//
//    @Bean
//    protected Tasklet chocoTaskletBatchEx1Tasklet1() {
//        return (contribution, chunkContext) -> {
//            log.info("chocoTaskletBatchEx1Tasklet1 execute.");
//            return RepeatStatus.FINISHED;
//        };
//    }
//
//    @Bean
//    protected Tasklet chocoTaskletBatchEx1Tasklet2() {
//        return (contribution, chunkContext) -> {
//            log.info("chocoTaskletBatchEx1Tasklet2 execute.");
//            return RepeatStatus.FINISHED;
//        };
//    }
}
