package com.choco.batch.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ChocoBatchScheduler {
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    @Qualifier("chocoChunkBatchEx1Job")
    private Job chocoChunkBatchEx1Job;

    @Scheduled(fixedRate = 10000)
    public void runChocoChunkBatchEx1Job() {
        try {
            jobLauncher.run(chocoChunkBatchEx1Job, new JobParameters());
        } catch (JobExecutionAlreadyRunningException e) {
            log.error("[Choco Batch Fail] chocoChunkBatchEx1Job failed (JobExecutionAlreadyRunningException).", e);
        } catch (JobRestartException e) {
            log.error("[Choco Batch Fail] chocoChunkBatchEx1Job failed (JobRestartException).", e);
        } catch (JobInstanceAlreadyCompleteException e) {
            log.error("[Choco Batch Fail] chocoChunkBatchEx1Job failed (JobInstanceAlreadyCompleteException).", e);
        } catch (JobParametersInvalidException e) {
            log.error("[Choco Batch Fail] chocoChunkBatchEx1Job failed (JobParametersInvalidException).", e);
        }
    }
}
