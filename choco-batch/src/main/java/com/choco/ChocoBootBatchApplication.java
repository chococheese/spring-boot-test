package com.choco;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * http://jinhokwon.tistory.com/35 보고 따라하는 중.
 */
@Slf4j
@EnableScheduling
@SpringBootApplication
@EnableBatchProcessing
public class ChocoBootBatchApplication implements CommandLineRunner {
    @PostConstruct
    public void init() {
        log.info("ChocoBootBatchApplication init.");
    }

    @PreDestroy
    public void onDestroy() {
        log.info("ChocoBootBatchApplication onDestroy.");
    }

    @Override
    public void run(String... args) {

    }

    public static void main(String[] args) {
        log.info("ChocoBootBatchApplication start.");

        SpringApplication.run(ChocoBootBatchApplication.class, args);

        log.info("ChocoBootBatchApplication end.");
    }
}
