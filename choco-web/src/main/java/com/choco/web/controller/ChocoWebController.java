package com.choco.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.InetAddress;

@Slf4j
@Controller
public class ChocoWebController {
    @RequestMapping("/")
    public String hello(Model model) throws Exception {
        String serverIp = InetAddress.getLocalHost().getHostAddress();

        model.addAttribute("helloMessage", "hello choco's world~!");
        model.addAttribute("serverIp", serverIp);

        return "index";
    }
}
