package com.choco.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

@Slf4j
@Controller
public class ChocoApiController {
    @Value("${spring.active.profile}")
    private String springActiveProfile;

    @ResponseBody
    @RequestMapping("/getThreadMXBeanInfo")
    public String getThreadMXBeanInfo() {
        ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();
        long[] threadIds = mxBean.getAllThreadIds();
        ThreadInfo[] threadInfos = mxBean.getThreadInfo(threadIds);

        StringBuilder stringBuilder = new StringBuilder();
        for (ThreadInfo threadInfo : threadInfos) {
            stringBuilder.append("thread name : ").append(threadInfo.getThreadName()).append(", ")
                    .append("blocked count : ").append(threadInfo.getBlockedCount()).append(", ")
                    .append("blocked time : ").append(threadInfo.getBlockedTime()).append(", ")
                    .append("waited count : ").append(threadInfo.getWaitedCount()).append(", ")
                    .append("waited time : ").append(threadInfo.getWaitedTime()).append(", ")
                    .append("\n");
        }

        return stringBuilder.toString();
    }

    @ResponseBody
    @RequestMapping("/activeProfile")
    public String getHello() {
        return ObjectUtils.defaultIfNull(springActiveProfile, "Profile does not exists.");
    }
}
