package com.choco.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * @see <a href="https://homoefficio.github.io/2016/11/19/Spring-Data-JPA-%EC%97%90%EC%84%9C-Java8-Date-Time-JSR-310-%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0/">Spring Data JPA 에서 Java8 Date-Time(JSR-310) 사용하기</a>
 */
@Slf4j
@EntityScan(
        basePackageClasses = {Jsr310JpaConverters.class},
        basePackages = {"com.choco"}
)
@SpringBootApplication
public class ChocoApi implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(ChocoApi.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        log.info("ChocoApi run() run run~~");
    }
}
